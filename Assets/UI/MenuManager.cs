using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    public GameObject Menu;
    public GameObject SureExit;
    public GameObject SureRestart;
    public GameObject Settings;
    public AudioMixer audioMixer;
    public Slider MVolume;
    public Slider MsVolume;
    public Slider EVolume;

    public void onSureExit()
    {
        Menu.SetActive(false);
        SureRestart.SetActive(false);
        Settings.SetActive(false);
        SureExit.SetActive(true);
    }
    
    public void onSureRestart()
    {
        Menu.SetActive(false);
        SureExit.SetActive(false);
        Settings.SetActive(false);
        SureRestart.SetActive(true);
    }

    public void onSettings()
    {
        Menu.SetActive(false);
        SureExit.SetActive(false);
        SureRestart.SetActive(false);
        Settings.SetActive(true);
    }

    public void yesExit()
    {
        Application.Quit();
    }

    public void toMainMenu()
    {
        SureExit.SetActive(false);
        SureRestart.SetActive(false);
        Settings.SetActive(false);
        Menu.SetActive(true);
    }

    public void yesRestart()
    {
        SceneManager.LoadScene("Main");
    }

    public void onMaster()
    {
        audioMixer.SetFloat("Master", MVolume.value);
    }

    public void onEffects()
    {
        audioMixer.SetFloat("Effects", EVolume.value);
    }

    public void onMusic()
    {
        audioMixer.SetFloat("Music", MsVolume.value);
    }
}
