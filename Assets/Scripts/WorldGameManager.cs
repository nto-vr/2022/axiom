using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WorldGameManager : MonoBehaviour
{
    public GameObject DangerLight;
    public AudioSource dangerSound;
    public List<DoorOpener> doors;
    public List<GameObject> lights;
    public List<GameObject> level1;
    public List<GameObject> level2;
    public int destroyed;
    public List<DamageMaterial> damages;
    public int damageLevel = 0;
    public AudioSource backgroundSound;
    public AudioSource powerOffSound;
    private bool lightsState = false;
    public ChangeHolo hologram;
    public AudioSource siren;

    // Start is called before the first frame update
    void Start()
    {
        destroyed = 0;
        foreach (var obj in level1)
        {
            obj.SetActive(false);
        }
        foreach (var obj in level2)
        {
            obj.SetActive(false);
        }


        StartCoroutine(Danger());
       
    }

    IEnumerator Danger()
    {
        yield return new WaitForSeconds(10);
        dangerSound.Play();
        while(true)
        {
            DangerLight.SetActive(true);
            yield return new WaitForSeconds(1);
            DangerLight.SetActive(false);
            yield return new WaitForSeconds(1);
        }
    }

    IEnumerator Doors()
    {
        while(true)
        {
            yield return new WaitForSeconds(Random.Range(30, 60));
            doors[Random.Range(0, doors.Count)].Disable();
        }

    }

    IEnumerator Lights()
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(50, 100));
            foreach (var light in lights)
            {
                light.SetActive(false);
            }
        }



    }

    public void OnRockCollision()
    {
        Debug.Log("Crash");
        damageLevel++;
        if(damageLevel == 1)
        {
            Level1();
        }
        if(damageLevel == 2)
        {
            Level2();
        }
        if(damageLevel == 3)
        {
            CrossScene.destroyed = destroyed;
            SceneManager.LoadScene("GameOver");
        }
        
    }
   

    public void ToogleLights()
    {
        if(lightsState)
        {
            lightsOff();
        }
        else
        {
            lightsOn();
        }
    }

    public void ProjectileDestroyed()
    {
        destroyed++;

    }

    public void lightsOff()
    {
        if (!lightsState)
            return;
        lightsState = false;
        backgroundSound.Pause();
        powerOffSound.Play();
        foreach (var light in lights)
        {
            light.SetActive(false);
        }
    }

    public void lightsOn()
    {
        if (lightsState)
            return;
        lightsState = true;
        backgroundSound.Play();
        foreach (var light in lights)
        {
            light.SetActive(true);
        }
    }

    public void Level1()
    {
        
        foreach (var obj in level1)
        {
            obj.SetActive(true);
        }
        foreach (var obj in damages)
        {
            obj.Level1();
        }
        lightsOff();
        hologram.level1();

    }

    public void Level2()
    {
        siren.Play();
        foreach (var door in doors)
        {
            door.Disable();
        }
        foreach (var obj in level2)
        {
            obj.SetActive(true);
        }
        foreach (var obj in damages)
        {
            obj.Level2();
        }
        hologram.level2();
        StartCoroutine(Doors());
        lightsOff();
    }
   
}
