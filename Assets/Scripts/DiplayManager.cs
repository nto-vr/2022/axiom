using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DiplayManager : MonoBehaviour
{
    public Text time;
    public Text asteroids;
    public RockController asteroid;
    public AsteroidGnerator asg;
    public GameObject canv;
    public List<GameObject> ase = new List<GameObject>();
    public float T;
    private int i = 0;

    void Start()
    {
        T = asteroid.FlightTime;
    }
    void Update()
    {
        i = 0;
        T -= Time.deltaTime;
        time.text = ((int) T).ToString();
        asteroids.text = asg.destr.ToString();
        GameObject[] ast;
        ast = GameObject.FindGameObjectsWithTag("Rock");
        foreach(GameObject asst in ast)
        {
            ase[i].GetComponent<Image>().rectTransform.localPosition = new Vector3(asst.transform.position.x, asst.transform.position.z, 0);
            i++;
        }
    }
}
