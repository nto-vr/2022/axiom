using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneLoader : MonoBehaviour
{
    AsyncOperation loadingOperation;
    public Slider progressBar;
    bool started = false;

    private void Awake()
    {
        // DontDestroyOnLoad(gameObject);
    }

    void Start()
    {
        StartCoroutine(NextLevelAfterWait());
        
    }

    // Update is called once per frame
    void Update()
    {
        if (started)
        {
            progressBar.value = Mathf.Clamp01(loadingOperation.progress / 0.9f);
            if (loadingOperation.isDone)
            {
            }
        }
        
            
            
    }

    IEnumerator NextLevelAfterWait()
    {
        yield return new WaitForSeconds(5.0f);
        loadingOperation = SceneManager.LoadSceneAsync(1);
        started = true;
    }
}
