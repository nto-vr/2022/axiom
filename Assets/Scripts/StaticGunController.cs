using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StaticGunController : MonoBehaviour
{
    public SmartSocket batterySocket;

    public List<GameObject> powerableParts;

    public Material powered;

    public Material unpowered;

    public Animator batteryAnimator;

    private BatteryManager battery;
    public GameObject ProjectilePrefab;
    public AudioSource audio;
    public Transform shootTransform;
    public float projectileSpeed = 5f;
    public void ToogleBatterySocket()
    {

        if (batteryAnimator.GetBool("Switch1"))
            CloseSocket();
        else
            OpenSocket();

    }



    public void OpenSocket()
    {
        batteryAnimator.SetBool("Switch1", true);
        foreach (var part in powerableParts)
        {
            part.GetComponent<Renderer>().material = unpowered;
        }
    }

    public void CloseSocket()
    {
        batteryAnimator.SetBool("Switch1", false);
        Debug.Log("close");
        Debug.Log(batterySocket.selectedObject);
        if (batterySocket.selectedObject != null)
        {
            battery = batterySocket.selectedObject.GetComponent<BatteryManager>();
            Debug.Log(battery);
            if (battery != null && battery.charged != 0)
            {
                foreach (var part in powerableParts)
                {
                    part.GetComponent<Renderer>().material = powered;
                }
            }
            else
            {
                foreach (var part in powerableParts)
                {
                    part.GetComponent<Renderer>().material = unpowered;
                }
            }
        }
        else
        {
            foreach (var part in powerableParts)
            {
                part.GetComponent<Renderer>().material = unpowered;
            }
        }

    }
    

    public void Shoot()
    {
        if (battery == null || battery.charged == 0)
            return;
        battery.Discharge(20);
        if (battery.charged == 0)
        {
            foreach (var part in powerableParts)
            {
                part.GetComponent<Renderer>().material = unpowered;
            }
        }
        audio.Play();
        var projectile = GameObject.Instantiate(ProjectilePrefab, shootTransform.position, Quaternion.Euler(shootTransform.eulerAngles));
        projectile.GetComponent<Rigidbody>().velocity = (shootTransform.forward).normalized * projectileSpeed;
    }

}
