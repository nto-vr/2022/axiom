using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.XR;
using UnityEngine.XR.Interaction.Toolkit;

public class ConstructableGunController : MonoBehaviour
{
    public SmartSocket batterySocket;

    public List<GameObject> powerableParts;

    public Material powered;

    public Material unpowered;

    public Animator batteryAnimator;

    private BatteryManager battery;

    public SmartSocket baseSocket;
    public SmartSocket barrelSocket1;
    public SmartSocket barrelSocket2;
    public Transform Tower;

    public Transform ForwardSource;
    public XRController controller;
    public GameObject ProjectilePrefab;
    public AudioSource audio;
    public float projectileSpeed = 5f;
    public GameObject PlayerOrigin;
    public GameObject XROrigin;
    public Text aim;
    
    private bool ready = false;
    private bool m_isActive = false;

    // Update is called one per frame

    public void ToogleBatterySocket()
    {
        
        if (batteryAnimator.GetBool("Switch1"))
            CloseSocket();
        else
            OpenSocket();
        
    }

    void Start()
    {
        Deactivate();
    }

    private void Update()
    {
        if (barrelSocket1.selectedObject != null &&
            barrelSocket2.selectedObject != null &&
            baseSocket.selectedObject != null &&
            battery != null &&
            battery.charged != 0 &&
            !batteryAnimator.GetBool("Switch1")
            )
        {
            
            ready = true;
        }
        else
        {
           ready = false;
        }

        if (m_isActive)
        {
            Tower.rotation = Quaternion.LookRotation(ForwardSource.up, ForwardSource.forward);
            RaycastHit hit;
            Physics.Raycast(Tower.position + Tower.up * 5, Tower.up, out hit, 500f);
            if (hit.collider != null)
                aim.color = Color.green;
            else
                aim.color = Color.red;
                
            
        }
            

        bool buttonPressed;
        controller.inputDevice.TryGetFeatureValue(CommonUsages.secondaryButton, out buttonPressed);

        if (buttonPressed)
        {
            Deactivate();
        }

    }



    public void OpenSocket()
    {
        Debug.Log("OPen");
        batteryAnimator.SetBool("Switch1", true);
        foreach (var part in powerableParts)
        {
            part.GetComponent<Renderer>().material = unpowered;
        }
    }
    
    public void CloseSocket()
    {
        batteryAnimator.SetBool("Switch1", false);
        Debug.Log("close");
        Debug.Log(batterySocket.selectedObject);
        if (batterySocket.selectedObject != null)
        {
            battery = batterySocket.selectedObject.GetComponent<BatteryManager>();
            Debug.Log(battery);
            if (battery != null && battery.charged != 0)
            {
                foreach (var part in powerableParts)
                {
                    part.GetComponent<Renderer>().material = powered;
                }
            }
            else
            {
                foreach (var part in powerableParts)
                {
                    part.GetComponent<Renderer>().material = unpowered;
                }
            }
        }
        else
        {
            foreach (var part in powerableParts)
            {
                part.GetComponent<Renderer>().material = unpowered;
            }
        }
        
    }

    public void Activate()
    {
        if (!ready) return;
        controller.enabled = true;
        XROrigin.SetActive(true);
        PlayerOrigin.SetActive(false);
        m_isActive = true;
        Debug.Log("activate");
    }

    public void Deactivate()
    {
        controller.enabled = false;
        XROrigin.SetActive(false);
        PlayerOrigin.SetActive(true);
        m_isActive = false;
    }

    public void Shoot()
    {
        if (!ready) return;
        battery.Discharge(50);
        audio.Play();
        var projectile = GameObject.Instantiate(ProjectilePrefab, Tower.position, Quaternion.LookRotation(Tower.up, Tower.forward));
        projectile.GetComponent<Rigidbody>().velocity = Tower.up.normalized * projectileSpeed;

    }
}
